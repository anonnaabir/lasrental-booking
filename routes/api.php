<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookingController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CarsController;
use App\Http\Controllers\SettingsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Post Requests
Route::post('/booking/create', BookingController::class . '@booking');
Route::post('/cars/price/set', BookingController::class . '@set_car_price');
Route::post('/post/create', PostController::class . '@create_post');
Route::post('/post/delete', PostController::class . '@delete_post');
Route::post('/post/delete', PostController::class . '@delete_post');
Route::post('/car/upload/image', CarsController::class . '@upload_car_image');
Route::post('/car/create', CarsController::class . '@add_car');
Route::post('/car/delete', CarsController::class . '@delete_car');
Route::post('/settings/update', SettingsController::class . '@update_settings');

// Get Requests
Route::get('/booking/get/all', BookingController::class . '@get_all_bookings');
Route::get('/cardata', BookingController::class . '@car_data');
Route::get('/cars/{pickup_date}', BookingController::class . '@get_cars');
Route::get('/cars/all', BookingController::class . '@get_all_cars');
Route::get('/post/get/all', PostController::class . '@get_all_posts');
Route::get('/post/get/{slug}', PostController::class . '@get_post_by_slug');
Route::get('/settings/get', SettingsController::class . '@get_settings');
Route::get('/car/get/{slug}', CarsController::class . '@get_car_by_slug');

// Route::get('/booking/get', BookingController::class . '@get_booking');
// Route::get('/booking/get/{pickup_date}', BookingController::class . '@get_booking');

