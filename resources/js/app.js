import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { createRouter, createWebHistory } from 'vue-router';

import VCalendar from 'v-calendar';
import 'v-calendar/style.css';


import PrimeVue from 'primevue/config';
import ToastService from 'primevue/toastservice';
import "primevue/resources/themes/lara-light-indigo/theme.css";

import App from './Vue/App.vue';
import Pickup from './Vue/Pickup.vue';
import Dropoff from './Vue/Dropoff.vue';
import Carlist from './Vue/Carlist.vue';
import BookingCost from './Vue/BookingCost.vue';
import PersonalDetails from './Vue/PersonalDetails.vue';

const routes = [
  { path: '/booking', name: 'home', component: Pickup },
  { path: '/dropoff', name: 'dropoff', component: Dropoff },
  { path: '/carlist', name: 'carlist', component: Carlist },
  { path: '/bookingcost', name: 'bookingcost', component: BookingCost },
  { path: '/personal-details', name: 'personaldetails', component: PersonalDetails }
];

// const routes = [
//     { path: '/booking', component: Pickup },
//     { path: '/dropoff', component: Dropoff },
//   ];

const router = createRouter({
  history: createWebHistory(), 
  routes,
});

const pinia = createPinia()

const app = createApp(App);
app.use(router);
app.use(VCalendar, {});
app.use(PrimeVue);
app.use(ToastService);
app.use(pinia);

// Mount the app
app.mount('#app');
