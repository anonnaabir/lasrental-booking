// stores/counter.js
import { defineStore } from 'pinia';

export const useBookingStore = defineStore('BookingStore', {
  state: () => {
    return { 
      pickupdate: null,
      dropoffdate: null,
      bookingdays: null,
      availablecars: null,
      unavailablecars: null,
      selectedcar : null,
      carcost: null,
      totalcost: null,
    }
  },
  actions: {
    
  },
})