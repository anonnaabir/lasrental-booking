import { createApp, h } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import { S3Client, AbortMultipartUploadCommand } from "@aws-sdk/client-s3";
import { createPinia } from 'pinia';
import Alpine from 'alpinejs';
import PrimeVue from 'primevue/config';
import "primevue/resources/themes/lara-light-indigo/theme.css";
import "primevue/resources/primevue.min.css";
import ToastService from 'primevue/toastservice';
import ConfirmationService from 'primevue/confirmationservice';

import Admin from './Admin.vue';
import Cars from './Cars.vue';
import Dashboard from './Dashboard.vue';
import Bookings from './Bookings.vue';
import GeoPage from './GeoPage.vue';
import Settings from './Settings.vue';

const routes = [
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/cars', name: 'cars', component: Cars },
  { path: '/bookings', name: 'Bookings', component: Bookings },
  { path: '/geo-page', name: 'GeoPage', component: GeoPage },
  { path: '/settings', name: 'Settings', component: Settings },
];

const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});

const pinia = createPinia();

const app = createApp({
  render: () => h(Admin),
});

window.Alpine = Alpine;

Alpine.start();

// const s3Client = new S3Client({
//   region: 'us-west-2',
//   credentials: {
//     accessKeyId: 'AKIAWPP5TZHRTYYUNJ2V',
//     secretAccessKey: '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'
//   }
// });

// AWS.config.update({
//   accessKeyId: 'AKIAWPP5TZHRTYYUNJ2V',
//   secretAccessKey: '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',
//   region: 'us-west-2'
// });

app.use(router);
app.use(PrimeVue);
app.use(ConfirmationService);
app.use(ToastService);
app.use(pinia);
app.mount('#lsadmin');