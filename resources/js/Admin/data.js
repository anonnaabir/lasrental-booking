import { defineStore } from 'pinia';

export const useAdminStore = defineStore('AdminStore', {
  state: () => {
    return { 
      cars: null,
    }
  },
  actions: {
    
  },
})