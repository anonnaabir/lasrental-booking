<?php

namespace App\Http\Controllers;
use Illuminate\Support\Str;
use App\Models\PostModel;
use Illuminate\Http\Request;

class PostController extends Controller {
    
    public function get_all_posts() {
        $posts = PostModel::all();
        return response()->json($posts);
    }

    public function get_post_by_slug($slug) {
        $post = PostModel::where('slug', $slug)->first();
        if (!$post) {
            return response()->json(['message' => 'Post not found'], 404);
        }
        return response()->json($post);
    }

    public function create_post(Request $request) {
        $data = $request->validate([
            'title' => 'required|string',
            'body' => 'nullable|string',
            'location_name' => 'nullable|string',
            'state' => 'nullable|string',
            'phone' => 'nullable|string',
            'city_information' => 'nullable|string',
        ]);
    
        // Replace placeholders in the body with actual values
        if ($data['body']) {
            $placeholders = [
                '{{location_name}}' => $data['location_name'] ?? '',
                '{{state}}' => $data['state'] ?? '',
                '{{phone}}' => $data['phone'] ?? '',
            ];
    
            $data['body'] = strtr($data['body'], $placeholders);
        }
    
        // You can generate a slug based on the title or use any other method you prefer
        $slug = Str::slug($data['title']);
    
        // Create a new post
        $post = new PostModel([
            'slug' => $slug,
            'title' => $data['title'],
            'body' => $data['body'] ?? null,
            'location_name' => $data['location_name'] ?? null,
            'state' => $data['state'] ?? null,
            'phone' => $data['phone'] ?? null,
            'city_information' => $data['city_information'] ?? null,
        ]);
    
        $post->save();
    
        // Return a JSON response
        return response()->json(['message' => 'Post created successfully', 'post' => $post], 201);
    }


    public function delete_post(Request $request) {
        $post = PostModel::find($request->id);
    
        if (!$post) {
            return response()->json(['message' => 'Post not found'], 404);
        }

        $post->delete();
        
        return response()->json(['message' => 'Post deleted successfully'], 200);
    }

}
