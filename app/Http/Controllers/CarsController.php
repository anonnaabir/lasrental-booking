<?php


namespace App\Http\Controllers;
use App\Models\CarsModel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;


class CarsController extends Controller {

    public function upload_car_image(Request $request){
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ]);

        // Store the uploaded image in the public directory
        $imagePath = $request->file('image')->store('uploads', 'public');

        // // You can save the image path to your database if needed
        // $image = new Image;
        // $image->path = $imagePath;
        // $image->save();

        return response()->json(asset('storage/' . $imagePath),200, [], JSON_UNESCAPED_SLASHES);

    }


    public function add_car(Request $request) {
        // Validate the request data
        $data = $request->validate([
            'name' => 'required',
            'cost' => 'nullable',
            'image' => 'nullable',
            'description' => 'nullable',
            'short_description' => 'nullable',
            'long_description' => 'nullable',
            'brand' => 'nullable',
            'model' => 'nullable',
            'body' => 'nullable',
            'seats' => 'nullable',
            'color' => 'nullable',
            'engine' => 'nullable',
            'transmission' => 'nullable',
            'mph' => 'nullable',
            'hp' => 'nullable',
            'drivetrain' => 'nullable',
            'torque' => 'nullable'
        ]);
        
        $slug = Str::slug($data['name']);
    
        // Create a new car model
        $car = new CarsModel([
            'slug' => $slug,
            'name' => $data['name'],
            'cost' => $data['cost'],
            'image' => $data['image'],
            'description' => $data['description'],
            'short_description' => $data['short_description'],
            'long_description' => $data['long_description'],
            'brand' => $data['brand'],
            'model' => $data['model'],
            'body' => $data['body'],
            'seats' => $data['seats'],  
            'color' => $data['color'],
            'engine' => $data['engine'],
            'transmission' => $data['transmission'],
            '0_60 MPH' => $data['mph'],
            'horsepower' => $data['hp'],
            'drivetrain' => $data['drivetrain'],
            'torque' => $data['torque']
        ]);
    
        // Save the car model to the database
        $car->save();
    
        // Return a JSON response
        return response()->json([
            'message' => 'Car added successfully',
            'car' => $car
        ]);
    }
    
    public function delete_car(Request $request) {
        // $data = $request->validate([
        //     'slug' => 'required|exists:cars_models,slug',
        // ]);
    
        $car = CarsModel::where('slug', $request->slug)->first();
    
        if (!$car) {
            return response()->json([
                'message' => 'Car not found',
            ], 404);
        }
    
        $car->delete();
    
        return response()->json([
            'message' => 'Car deleted successfully',
        ]);
    }
    

    public function get_car_by_slug(Request $request, $slug) {
        $car = CarsModel::where('slug', $slug)->first();
        
        if (!$car) {
            return response()->json(['message' => 'Car not found'], 404);
        }
        
        return response()->json($car);
    }    

}
