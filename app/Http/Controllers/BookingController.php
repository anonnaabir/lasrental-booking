<?php

namespace App\Http\Controllers;
use App\Models\BookingModel;
use App\Models\User;
use App\Models\CarsModel;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Notification;
use App\Notifications\BookingNotification;


class BookingController extends Controller
{
    public function index() {
        // get booking by date
        
        $booking = BookingModel::where('pickup_date', '>=', date('Y-m-d'))->get();
        // dd($booking);
        return view('booking');
    }

    public function booking(Request $request) {
        $user = User::first();

        $bookingModel = new BookingModel();
        $bookingModel->name = $request->name;
        $bookingModel->email = $request->email;
        $bookingModel->phone = $request->phone;
        $bookingModel->pickup_date = $request->pickup_date;
        $bookingModel->dropoff_date = $request->dropoff_date;
        $bookingModel->selected_car = $request->selected_car;
        // $bookingModel->booking_hour = $request->booking_hour;
        $bookingModel->price = $request->price;        

        $bookingModel->save();

        $bookingData = BookingModel::latest('id')->first();
        $user->notify(new BookingNotification($bookingData));
        
        return response()->json([
            'status' => 'success',
            'status_code' => 200,
            // 'data' => $bookingModel
        ]);


        // $bookingModel->booking_address = $request->booking_address;
        // $bookingModel->booking_city = $request->booking_city;
        // $bookingModel->booking_state = $request->booking_state;
        // $bookingModel->booking_country = $request->booking_country;
        // $bookingModel->booking_phone = $request->booking_phone;
        // $bookingModel->booking_email = $request->booking_email;
        // $bookingModel->booking_id = $request->booking_id;
        // $bookingModel->booking_status = $request->booking_status;
    }

    public function get_booking($pickup_date) {
        // $pickup_date =  $request->pickup_date;
        $booking = BookingModel::where('pickup_date', $pickup_date)->get(['selected_car']);
        $cars = $booking->pluck('selected_car')->toArray();
        
        // $all = [
        //     'selected_car' => $cars,
        //     'availability' => 'Not Available',
        // ];

        // dd($cars);
        // convert object to array
        // $cars = json_decode(json_encode($booking), true);
        // foreach ($booking as $key => $value) {
        //     $cars = [
        //         'selected_car' => $value->selected_car,
        //         'availability' => 'Not Available',
        //     ];
            
        // }

        return response()->json([
            $cars
        ]);

        // get all booking by date
        // $booking = BookingModel::all();


        // if (isset($booking->pickup_date)) {
        //     // $selected_car = $booking->selected_car;
            
        //     return response()->json([
        //         // 'status' => 'success',
        //         // 'id' => $booking->id,
        //         // 'pickup_date' => $booking->pickup_date,
        //         // 'selected_car' => $booking->selected_car,
        //         // 'availability' => 'Not Available',
        //         $booking
        //     ]);
        // }

        // else {
        //     return response()->json([
        //         'status' => 'No Booking',
        //     ]);
        // }

        // if (!$booking) {
        //     return response()->json([
        //         'status' => 'error',
        //         'status_code' => 404,
        //         'message' => 'Booking not found'
        //     ]);
        // }

        // else {
        //     return response()->json([
        //         'status' => 'success',
        //         'status_code' => 200,
        //         'data' => $booking
        //     ]);
        // }

    }

    public function get_cars($pickup_date) {
        $booking = BookingModel::where('pickup_date', $pickup_date)->get(['selected_car']);
        $cars = CarsModel::all();
        // remove selected car from cars
        $available_cars = $cars->whereNotIn('slug', $booking->pluck('selected_car')->toArray());

        // get unavailable cars
        $unavailable_cars = $cars->whereIn('slug', $booking->pluck('selected_car')->toArray());
        return response()->json([
            'available_cars' => $available_cars,
            'unavailable_cars' => $unavailable_cars,
            // $booking
        ]);
    }

    public function get_all_cars() {
        $cars = CarsModel::all();
        return response()->json([
            'cars' => $cars,
        ]);
    }

    public function car_data(Request $request) {
        $car = CarsModel::where('slug', $request->slug)->first();
        if (!$car) {
            return response()->json([
                'status' => 'error',
                'status_code' => 404,
                'message' => 'Car not found'
            ]);
        }

        else {
            return response()->json([
                'status' => 'success',
                'status_code' => 200,
                'data' => $car
            ]);
        }
    }

    public function get_all_bookings(Request $request) {
        $bookings = BookingModel::all();
        return response()->json($bookings);
    }    

    public function set_car_price(Request $request) {
        $data = json_decode($request->getContent(), true);
    
        $slug = $data['slug'];
        $newPrice = $data['cost'];
    
        if ($newPrice !== null) {
            // Update the cost table with the new price for the specific car
            CarsModel::where('slug', $slug)->update(['cost' => $newPrice]);
    
            return response()->json([
                'status' => 'success',
                'status_code' => 200,
                'message' => 'Car price updated successfully'
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'status_code' => 400,
                'message' => 'Invalid slug'
            ]);
        }
    }

}
