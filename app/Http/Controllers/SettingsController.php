<?php

namespace App\Http\Controllers;
use App\Models\SettingsModel;
use Illuminate\Http\Request;

class SettingsController extends Controller {

    public function get_settings(Request $request) {
        $settings = SettingsModel::first();
        return response()->json($settings);
    }

    public function update_settings(Request $request) {
        $validatedData = $request->validate([
            'herotext' => 'nullable|string',
            'videotitle' => 'nullable|string',
            'videodesc' => 'nullable|string',
            'carstitle' => 'nullable|string',
            'carsdesc' => 'nullable|string',
            'phone' => 'nullable|string',
        ]);
    
        // Use updateOrInsert to update the settings or insert a new row if it doesn't exist
        SettingsModel::updateOrInsert(
            [],
            $validatedData
        );
    
        return response()->json(['message' => 'Settings updated successfully']);
    }    
}