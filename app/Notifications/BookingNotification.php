<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BookingNotification extends Notification
{
    use Queueable;
    private $bookingData;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($bookingData) {
        $this->bookingData = $bookingData;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable) {
        return (new MailMessage)
                    ->subject('Booking Notification')
                    ->line('')
                    ->greeting('New booking has placed')
                    ->line('Customer Name: '.$this->bookingData['name'])
                    ->line('Customer Email: '.$this->bookingData['email'])
                    ->line('Customer Phone: '.$this->bookingData['phone'])
                    ->line('Pickup Date: '.$this->bookingData['pickup_date'])
                    ->line('Dropoff Date: '.$this->bookingData['dropoff_date'])
                    ->line('Selected Car: '.$this->bookingData['selected_car'])
                    ->line('Price: '.$this->bookingData['price'])
                    ->line(' ')
                    ->line(' ');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
