<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostModel extends Model
{
    use HasFactory;

    protected $table = 'posts';

    protected $fillable = [
        'slug', // Add 'slug' to the fillable attributes
        'title',
        'body',
        'location_name',
        'state',
        'phone',
        'city_information',
    ];
}