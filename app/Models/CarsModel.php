<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarsModel extends Model
{
    use HasFactory;
    protected $table = 'cars';

    protected $fillable = [
        'slug', // Add 'slug' to the fillable attributes
        'name',
        'description',
        'cost',
        'image',
        'short_description',
        'long_description',
        'brand',
        'model',
        'body',
        'seats',
        'color',
        'engine',
        'transmission',
        '0_60 MPH',
        'horsepower',
        'drivetrain',
        'torque'
    ];
}
