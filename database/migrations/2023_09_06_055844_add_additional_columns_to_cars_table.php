<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cars', function (Blueprint $table) {
            $table->longText('short_description')->nullable();
            $table->longText('long_description')->nullable();
            $table->string('brand',100)->nullable();
            $table->string('model',100)->nullable();
            $table->string('body',100)->nullable();
            $table->string('seats',50)->nullable();
            $table->string('color',50)->nullable();
            $table->string('engine',50)->nullable();
            $table->string('transmission',50)->nullable();
            $table->string('0_60 MPH',50)->nullable();
            $table->string('horsepower',50)->nullable();
            $table->string('drivetrain',50)->nullable();
            $table->string('torque',50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cars', function (Blueprint $table) {
            //
        });
    }
};
